Proper Orthogonal Decomposition based Reduced Order Modeling with TensorFlow

If you have found this useful, please cite:
"Integrating a reduced-order model server into the engineering design process." Advances in Engineering Software 90 (2015): 169-182.
